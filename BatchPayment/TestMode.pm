package Business::BatchPayment::TestMode;

=head1 DESCRIPTION

The 'test_mode' attribute.  A module that does this role is promising that
requests will not be submitted to a live account if test_mode is true.

=cut

use strict;
use Moose::Role;

has 'test_mode' => (
  is => 'rw',
  isa => 'Bool',
  default => 0,
);

1;
