package Business::BatchPayment::Transport;

use strict;
use Moose::Role;
with 'Business::BatchPayment::Debug';

requires 'upload';
requires 'download';

=head1 NAME

Business::BatchPayment::Transport

=head1 USAGE

Business::BatchPayment::Processor classes must have a 'transport' attribute
which does the Business::BatchPayment::Transport role (implements 'upload'
and 'download').  The B::BP::Processor class can implement 'default_transport'
to construct a B::BP::Transport object.

'upload' must take a scalar argument containing the (correctly formatted)
batch for upload.  On success, it may return nothing, or a scalar identifier
for the batch (such as a serial number) if the gateway requires one.

'download' takes the return value from 'upload' (if any) as its argument, 
and returns any number of scalars containing batch files.  If there are no 
batches available, it should return an empty list.

Both of these methods should die on error.

=cut

1;
