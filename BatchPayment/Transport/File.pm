package Business::BatchPayment::Transport::File;

=head2 File transport

The simplest case.  Takes two arguments, 'input' and 'output'.  These can 
be open filehandles or strings naming files.  If unspecified, they default 
to /dev/null.

=cut

use strict;
use Moose;
use IO::File;
with 'Business::BatchPayment::Transport';

has 'input' => (
  is => 'rw',
  isa => 'Maybe[FileHandle|Str]',
  default => sub {
    warn "no input passed to file transport; using /dev/null";
    '/dev/null'
  },
  #lazy => 1,
);

has 'output' => (
  is => 'rw',
  isa => 'Maybe[FileHandle|Str]',
  default => sub {
    warn "no output passed to file transport; using /dev/null";
    '/dev/null'
  },
  #lazy => 1,
);

sub upload {
  my $self = shift;
  my $text = shift;
  my $fh;
  if ( ref $self->output ) {
    $fh = $self->output;
  } else {
    $fh = IO::File->new();
    $fh->open($self->output,'>')
      or die "couldn't write to ".$self->output;
  }
  print $fh $text;
}

sub download {
  my $self = shift;
  my $fh;
  if ( ref $self->input ) {
    $fh = $self->input;
  } else {
    $fh = IO::File->new();
    $fh->open($self->input,'<')
      or die "couldn't read from ".$self->input;
  }
  local $/;
  my $text = <$fh>;
}

1;
