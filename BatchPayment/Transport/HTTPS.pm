package Business::BatchPayment::Transport::HTTPS;

=head2 HTTPS transport

Sends a request by HTTPS POST, and downloads the response the same way.
Options are 'server', 'port', 'get_path', 'put_path', optionally 
'content_type'.

=cut

use strict;
use Moose;
use Net::HTTPS::Any 0.10;
with 'Business::BatchPayment::Transport';

has [ qw( host port get_path put_path ) ] => ( 
  is => 'rw',
  isa => 'Str'
);

has 'content_type' => (
  is => 'rw',
  isa => 'Str',
  default => '', # application/x-www-form-urlencoded
);

sub https_post {
  my $self = shift;
  my $path = shift;
  my $content = shift;
  my %post = (
    host => $self->host,
    port => $self->port,
    path => $path,
    debug => ($self->debug > 3 ? 1 : 0),
    'Content-Type' => $self->content_type
  );
  if (ref $content and ref $content eq 'HASH') {
    $post{'args'} = $content;
  } else {
    $post{'content'} = $content;
  }

  warn "starting https_post...\n" if $self->debug;
  my ( $page, $response, %reply_headers ) = Net::HTTPS::Any::https_post(%post);

  warn "PAGE:\n$page\n\nRESPONSE:\n$response\n\n" if $self->debug >= 2;
  return ($page, $response, %reply_headers);
}

sub upload {
  my $self = shift;
  my $content = shift;
  $self->https_post($self->put_path, $content);
}

sub download {
  # will probably need to be overridden in most cases
  my $self = shift;
  my ($page, $response, %reply_headers) = $self->https_post($self->get_path);
  $page;
}

__PACKAGE__->meta->make_immutable;

1;
