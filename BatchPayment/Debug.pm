package Business::BatchPayment::Debug;

=head1 DESCRIPTION

The 'debug' attribute.  All Business::BatchPayment classes should support 
this.  $Business::BatchPayment::DEBUG can be set to turn on debugging 
globally.

=cut

use strict;
use Moose::Role;

has 'debug' => (
  is => 'rw',
  isa => 'Int',
  default => sub { $Business::BatchPayment::DEBUG || 0 },
  lazy => 1,
);

1;
