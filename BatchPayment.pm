package Business::BatchPayment;

use 5.006;
use strict;
use vars '$DEBUG';
use warnings;

use Class::MOP;
use Business::BatchPayment::Processor;
use Business::BatchPayment::Item;
use Business::BatchPayment::Transport;

$DEBUG = 0;

=head1 NAME

Business::BatchPayment - Batch-oriented payment processing

=cut

our $VERSION = '0.02';

=head1 SYNOPSIS

  use Business::BatchPayment;

  my %options = ( merchant_id => '00451', password => 'opensesame' );
  my $processor = Business::BatchPayment->create(MyGateway => %options);

  my @request;
  push @request, Business::BatchPayment->create(Item =>
    action        => 'payment', # as opposed to 'credit'
    payment_type  => 'CC', #credit card, or 'ECHECK' for check/ACH
    amount        => '49.95',
    tid           => '0001234', # transaction id, like a customer/order number
    card_number   => '1234123412341238',
    expiration    => '0100', # MM/YY
    # these fields are optional
    first_name    => 'John',
    last_name     => 'Doe',
    address       => '123 Main Street',
    address2      => 'Suite H',
    city          => 'Anytown',
    state         => 'CA',
    country       => 'US',
    zip           => '99015',
  ); # returns a Business::OnlinePayment::Item;

  $processor->submit(@request);

  # at some point in the future
  
  my @reply = $processor->receive();
  foreach my $item (@reply) {
    ... process items and record successful/failed payments
  }

  # if your processor uses processor_id...

  $processor->submit(@request);
  foreach (@request) {
    ... record process_id for that request
  }
  # later...
  my @reply = $processor->receive(@process_ids);

=head1 CLASS METHODS

=over 4

=item create MODULE[, OPTIONS ]

Loads Business::BatchPayment::MODULE, then attempts to call 
Business::BatchPayment::MODULE->new(OPTIONS).

=cut

# not a Moose method

sub create {
  my $class = shift;
  my $subclass = shift;
  $subclass = "Business::BatchPayment::$subclass";
  Class::MOP::load_class($subclass);
  $subclass->new(@_);
}

=back

=head1 AUTHOR

Mark Wells, C<< <mark at freeside.biz> >>

=head1 BUGS

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Business::BatchPayment

Commercial support is available from Freeside Internet Services,
L<http://www.freeside.biz>.

=head1 LICENSE AND COPYRIGHT

Copyright 2012 Mark Wells.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.


=cut

1; # End of Business::BatchPayment
