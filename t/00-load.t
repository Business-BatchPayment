#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Business::BatchPayment' ) || print "Bail out!\n";
}

diag( "Testing Business::BatchPayment $Business::BatchPayment::VERSION, Perl $], $^X" );
